from urllib import response
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

# This is where we put code to make the request


def find_pictures(search_term):
    url = "http://api.pexels.com/v1/search?query=" + search_term

    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)

    # this just gives us the content
    data = json.loads(response.content)
    # what do we want from our data, look at dat variable (can print them in insomina),
    # photos is their name
    photos = data["photos"]
    # return the photos, can put photos[0] if you want to specify just to choose the
    # first photo
    return photos


def get_weather_data(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ",US&limit=5&appid="
        + OPEN_WEATHER_API_KEY
    )

    response = requests.get(url)

    data = json.loads(response.content)[0]

    lon = data["lon"]
    lat = data["lat"]

    return {}
